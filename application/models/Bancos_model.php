<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bancos_model extends CI_Model
{
	public function consultar($id)
	{
		$this->db->from('bancos');
		$this->db->where('id', $id);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function eliminar($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('bancos');
	}

	public function listado()	
	{
		$this->db->from('bancos');
		$this->db->order_by('nombre', 'asc');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function modificar($id, $datos)
	{
		$this->db->where('id', $id);
		$this->db->update('bancos', $datos);
	}

	public function registrar($datos)
	{
		$this->db->insert('bancos', $datos);
	}

}

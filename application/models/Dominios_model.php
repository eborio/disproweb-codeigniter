<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dominios_model extends CI_Model
{
	public function consultar($id)
	{
		$this->db->from('dominios');
		$this->db->where('id', $id);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function eliminar($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('dominios');
	}

	public function listado()
	{
		$this->db->from('dominios');
		$this->db->order_by('extension', 'asc');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function listadoFecha($fecha_inicial, $fecha_final)
	{
		$this->db->from('dominio_pedido');
		$this->db->join('dominios', 'dominio_pedido.dominio_id = dominios.id');
		$this->db->where('fecha_expiracion >=', $fecha_inicial);
		$this->db->where('fecha_expiracion <=', $fecha_final);
		$this->db->order_by('fecha_expiracion', 'desc');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function modificar($id, $datos)
	{
		$this->db->where('id', $id);
		$this->db->update('dominios', $datos);
	}

	public function registrar($datos)
	{
		$this->db->insert('dominios', $datos);
	}
}

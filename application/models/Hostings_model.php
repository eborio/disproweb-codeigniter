<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hostings_model extends CI_Model
{
	public function consultar($id)
	{
		$this->db->from('planes');
		$this->db->where('id', $id);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function eliminar($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('planes');
	}

	public function listado($campo = 'nombre', $orden = 'asc')
	{
		$this->db->from('planes');
		$this->db->order_by($campo, $orden);
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function modificar($id, $datos)
	{
		$this->db->where('id', $id);
		$this->db->update('planes', $datos);
	}

	public function registrar($datos)
	{
		$this->db->insert('planes', $datos);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Interactua con la tabla de clientes
 */
class Clientes_model extends CI_Model
{
	public function consultar($id)
	{
		$this->db->from('clientes');
		$this->db->where('id', $id);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function listado()
	{
		$this->db->from('clientes');
		$this->db->order_by('usuario', 'asc');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	/**
	 * Realiza el login de usuarios
	 * 
	 * @param  string $usuario  Nombre de usuario
	 * @param  string $password Password del usuario
	 * @return bool
	 */
	public function login($usuario, $password)
	{
		$this->db->from('clientes');
		$this->db->where('usuario', $usuario);
		$this->db->where('password', $password);
		$consulta = $this->db->get();

		if ($consulta->num_rows() > 0) {
			$datos_cliente = $consulta->row();
			$this->session->set_userdata('cliente_usuario', $datos_cliente->usuario);
			$this->session->set_userdata('cliente_id', $datos_cliente->id);
			return true;
		} else {
			return false;
		}
	}

	public function modificar($id, $datos)
	{
		$this->db->where('id', $id);
		$this->db->update('clientes', $datos);
	}

	public function registrar($datos)
	{
		$this->db->insert('clientes', $datos);
		$cliente = $this->db->insert_id();
		return $cliente;
	}
}

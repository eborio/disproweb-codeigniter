<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagos_model extends CI_Model
{
	public function datosPedidos($pago_id)
	{
		$this->db->from('detalle_factura');
		$this->db->join('factura', 'detalle_factura.factura_id = factura.id');
		$this->db->join('pedidos', 'factura.pedidos_id = pedidos.id');
		$this->db->where('detalle_factura.pagos_id', $pago_id);
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function guardar($datos)
	{
		$this->db->insert('pagos', $datos);
		$pago = $this->db->insert_id();
		return $pago;
	}

	public function guardarDetallesFactura($datos)
	{
		$this->db->insert('detalle_factura', $datos);
	}

	public function listado($estado = null)
	{
		if (!is_null($estado)) {
			$this->db->where('estado_id', $estado);
		}
		$this->db->select('pagos.id, referencia_trans, fecha, monto_pagado, comentario, nombre, nro_cuenta, pagos.estado_id as estado_pago');
		$this->db->from('pagos');
		$this->db->order_by('fecha', 'desc');
		$this->db->group_by('id');
		$this->db->join('bancos', 'pagos.banco_id = bancos.id');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function listadoPorFecha($fecha_inicial, $fecha_final)
	{
		$this->db->select('pagos.id, referencia_trans, fecha, monto_pagado, comentario, nombre, nro_cuenta, pagos.estado_id as estado_pago');
		$this->db->from('pagos');
		$this->db->where('fecha >=', $fecha_inicial);
		$this->db->where('fecha <=', $fecha_final);
		$this->db->order_by('fecha', 'desc');
		$this->db->group_by('id');
		$this->db->join('bancos', 'pagos.banco_id = bancos.id');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function modificar($pago_id, $datos)
	{
		$this->db->where('id', $pago_id);
		$this->db->update('pagos', $datos);
	}

	public function pagosCliente($cliente)
	{
		$this->db->select('pagos.id, referencia_trans, pagos.fecha, monto_pagado, comentario, nombre, nro_cuenta, pagos.estado_id as estado_pago');
		$this->db->from('pagos');
		$this->db->order_by('fecha', 'desc');
		$this->db->join('bancos', 'pagos.banco_id = bancos.id');
		$this->db->join('detalle_factura', 'pagos.id = detalle_factura.pagos_id');
		$this->db->join('factura', 'detalle_factura.factura_id = factura.id');
		$this->db->join('pedidos', 'factura.pedidos_id = pedidos.id');
		$this->db->where('cliente_id', $cliente);
		$this->db->group_by('pagos.id');
		$consulta = $this->db->get();
		return $consulta->result();
	}
}

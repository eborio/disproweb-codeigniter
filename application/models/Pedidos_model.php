<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos_model extends CI_Model
{	
	public function consultar($pedido_id)
	{
		$this->db->from('pedidos');
		$this->db->where('id', $pedido_id);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function consultarDetallesDominios($pedido_id)
	{
		$this->db->from('dominio_pedido');
		$this->db->join('dominios', 'dominio_pedido.dominio_id = dominios.id');
		$this->db->where('pedido_id', $pedido_id);
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function consultarDetallesPlanes($pedido_id)
	{
		$this->db->from('planes_pedidos');
		$this->db->join('planes', 'planes_pedidos.planes_id = planes.id');
		$this->db->where('pedidos_id', $pedido_id);
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function guardar($datos)
	{
		$this->db->insert('pedidos', $datos);
		$pedido = $this->db->insert_id();
		return $pedido;
	}

	public function guardarDetallesDominios($datos)
	{
		$this->db->insert('dominio_pedido', $datos);
	}

	public function guardarDetallesHosting($datos)
	{
		$this->db->insert('planes_pedidos', $datos);
	}

	public function listado()
	{
		$this->db->select('pedidos.id, fecha, cedula, nombre, apellido');
		$this->db->from('pedidos');
		$this->db->join('clientes', 'pedidos.cliente_id = clientes.id');
		$consulta = $this->db->get();
		return $consulta->result();
	}
}

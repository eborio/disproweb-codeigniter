<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Iva_model extends CI_Model
{	
	public function consultar()
	{
		$this->db->from('iva');
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function modificar($id, $datos)
	{
		$this->db->where('id', $id);
		$this->db->update('iva', $datos);
	}
}

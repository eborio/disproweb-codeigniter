<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Niveles_model extends CI_Model
{
	public function listado()
	{
		$this->db->from('niveles');
		$this->db->order_by('nombre', 'asc');
		$consulta = $this->db->get();
		return $consulta->result();
	}
}

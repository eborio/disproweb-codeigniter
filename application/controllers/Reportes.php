<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'third_party/dompdf/autoload.inc.php';

use Dompdf\Dompdf;

class Reportes extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();	
		$this->load->model('Bancos_model');
		$this->load->model('Clientes_model');
		$this->load->model('Dominios_model');
		$this->load->model('Facturas_model');
		$this->load->model('Hostings_model');
		$this->load->model('Iva_model');
		$this->load->model('Pagos_model');
		$this->load->model('Pedidos_model');
		$this->load->model('Promociones_model');
		$this->load->model('Usuarios_model');
	}

	public function bancos()
	{
		$datos['bancos'] = $this->Bancos_model->listado();
		$datos['titulo'] = 'Lista de bancos';
		$datos['contenido'] = 'bancos/listado';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-bancos');
	}

	public function clientes()
	{
		$datos['clientes'] = $this->Clientes_model->listado();
		$datos['titulo'] = 'Lista de clientes';
		$datos['contenido'] = 'clientes/listado';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-clientes');
	}

	public function dominios()
	{
		$datos['dominios'] = $this->Dominios_model->listado();
		$datos['titulo'] = 'Lista de dominios';
		$datos['contenido'] = 'dominios/listado';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-dominios');
	}

	public function dominiosFecha()
	{
		$datos['titulo'] = 'Dominios por fecha';
		$datos['contenido'] = 'dominios/fecha';
		$this->load->view('administrador', $datos);
	}

	public function dominiosPorFecha()
	{
		$fecha_inicial = $this->input->post('fecha_inicial');
		$fecha_final = $this->input->post('fecha_final');

		$datos['dominios'] = $this->Dominios_model->listadoFecha($fecha_inicial, $fecha_final);
		$datos['titulo'] = 'Lista de dominios';
		$datos['contenido'] = 'dominios/listado_pdf';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-pagos');
	}

	public function exitoso($pago_id)
	{
		$pedidos = $this->Pagos_model->datosPedidos($pago_id);

		$datos_planes = array();
		foreach ($pedidos as $pedido) {
			$datos_planes[$pedido->pedidos_id] = $this->Pedidos_model->consultarDetallesPlanes($pedido->pedidos_id);
		}

		$datos_dominios = array();
		foreach ($pedidos as $pedido) {
			$datos_dominios[$pedido->pedidos_id] = $this->Pedidos_model->consultarDetallesDominios($pedido->pedidos_id);
		}

		$datos['pedidos'] = $pedidos;
		$datos['datos_planes'] = $datos_planes;
		$datos['datos_dominios'] = $datos_dominios;
		$datos['titulo'] = 'Pago exitoso';
		$datos['contenido'] = 'pagos/exitoso_pdf';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-pagos');
	}

	public function facturas($estado = null)
	{
		$facturas = $this->Facturas_model->listado($estado);
		$iva = $this->Iva_model->consultar();
		$totales_facturas = array();
		foreach ($facturas as $factura) {
			$datos_dominios = $this->Pedidos_model->consultarDetallesDominios($factura->pedido_id);

			$total_dominios = 0;
			foreach ($datos_dominios as $dominio) {
				if ($dominio->tipo == 'Registro') {
					$total_dominios += $dominio->precio_registro;
				}
				if ($dominio->tipo == 'Transferencia') {
					$total_dominios += $dominio->precio_transferencia;
				}
				if ($dominio->tipo == 'Propio') {
					$total_dominios += $dominio->precio_propio;
				}
			}

			$datos_planes = $this->Pedidos_model->consultarDetallesPlanes($factura->pedido_id);
			$total_planes = 0;
			foreach ($datos_planes as $plan) {
				if ($plan->ciclo_facturacion == 'Mensual') {
					$total_planes += $plan->precio_mensual;
				}
				if ($plan->ciclo_facturacion == 'Trimestral') {
					$total_planes += $plan->precio_trimestral;
				}
				if ($plan->ciclo_facturacion == 'Semestral') {
					$total_planes += $plan->precio_semestral;
				}
				if ($plan->ciclo_facturacion == 'Anual') {
					$total_planes += $plan->precio_anual;
				}
			}

			$subtotal = $total_dominios + $total_planes;
			$total_iva = ($subtotal - $factura->descuento) * ($iva->iva / 100);
			$data = array(
				'subtotal' => $subtotal,
				'iva' => $total_iva
				);
			$totales_facturas[$factura->id] = $data;
		}

		$datos['facturas'] = $facturas;
		$datos['totales_facturas'] = $totales_facturas;
		$datos['titulo'] = 'Lista de facturas';
		$datos['contenido'] = 'facturas/listado_reporte';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-facturas');
	}

	public function facturasFecha()
	{
		$datos['titulo'] = 'Facturas por fecha';
		$datos['contenido'] = 'facturas/fecha';
		$this->load->view('administrador', $datos);
	}

	public function facturasPorFecha()
	{
		$fecha_inicial = $this->input->post('fecha_inicial');
		$fecha_final = $this->input->post('fecha_final');

		$facturas = $this->Facturas_model->listadoPorFecha($fecha_inicial, $fecha_final);
		$iva = $this->Iva_model->consultar();
		$totales_facturas = array();
		foreach ($facturas as $factura) {
			$datos_dominios = $this->Pedidos_model->consultarDetallesDominios($factura->pedido_id);

			$total_dominios = 0;
			foreach ($datos_dominios as $dominio) {
				if ($dominio->tipo == 'Registro') {
					$total_dominios += $dominio->precio_registro;
				}
				if ($dominio->tipo == 'Transferencia') {
					$total_dominios += $dominio->precio_transferencia;
				}
				if ($dominio->tipo == 'Propio') {
					$total_dominios += $dominio->precio_propio;
				}
			}

			$datos_planes = $this->Pedidos_model->consultarDetallesPlanes($factura->pedido_id);
			$total_planes = 0;
			foreach ($datos_planes as $plan) {
				if ($plan->ciclo_facturacion == 'Mensual') {
					$total_planes += $plan->precio_mensual;
				}
				if ($plan->ciclo_facturacion == 'Trimestral') {
					$total_planes += $plan->precio_trimestral;
				}
				if ($plan->ciclo_facturacion == 'Semestral') {
					$total_planes += $plan->precio_semestral;
				}
				if ($plan->ciclo_facturacion == 'Anual') {
					$total_planes += $plan->precio_anual;
				}
			}

			$subtotal = $total_dominios + $total_planes;
			$total_iva = ($subtotal - $factura->descuento) * ($iva->iva / 100);
			$data = array(
				'subtotal' => $subtotal,
				'iva' => $total_iva
				);
			$totales_facturas[$factura->id] = $data;
		}

		$datos['facturas'] = $facturas;
		$datos['totales_facturas'] = $totales_facturas;
		$datos['titulo'] = 'Lista de facturas';
		$datos['contenido'] = 'facturas/listado_reporte';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-facturas');
	}

	public function facturaPdf($factura_id, $pedido_id)
	{
		$pedido = $this->Pedidos_model->consultar($pedido_id);
		$datos['mensaje'] = 'Su pedido se procesó exitosamente. Debe estar atento a su correo. Allí se le enviaran las credenciales de su cuenta cPanel. También se le enviará allí su factura en formato PDF.';

		$datos['detalles_dominios'] = $this->Pedidos_model->consultarDetallesDominios($pedido_id);
		$datos['detalles_planes'] = $this->Pedidos_model->consultarDetallesPlanes($pedido_id);
		$datos['cliente'] = $this->Clientes_model->consultar($pedido->cliente_id);
		$datos['factura'] = $this->Facturas_model->consultar($factura_id);
		$datos['iva'] = $this->Iva_model->consultar();
		$datos['titulo'] = 'Factura';
		$datos['contenido'] = 'pedidos/factura_pdf';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'factura-fdf');
	}

	public function pedidos()
	{
		$pedidos = $this->Pedidos_model->listado();

		$dominios = array();
		foreach ($pedidos as $pedido) {
			$dominios[$pedido->id] = $this->Pedidos_model->consultarDetallesDominios($pedido->id);
		}

		$planes = array();
		foreach ($pedidos as $pedido) {
			$planes[$pedido->id] = $this->Pedidos_model->consultarDetallesPlanes($pedido->id);
		}

		$datos['pedidos'] = $pedidos;
		$datos['dominios'] = $dominios;
		$datos['planes'] = $planes;
		$datos['titulo'] = 'Listado de pedidos';
		$datos['contenido'] = 'pedidos/listado';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-planes');
	}

	public function pagosFecha()
	{
		$datos['titulo'] = 'Pagos por fecha';
		$datos['contenido'] = 'pagos/fecha';
		$this->load->view('administrador', $datos);
	}

	public function pagosPorFecha()
	{
		$fecha_inicial = $this->input->post('fecha_inicial');
		$fecha_final = $this->input->post('fecha_final');
		$pagos = $this->Pagos_model->listadoPorFecha($fecha_inicial, $fecha_final);

		$facturas = array();
		foreach ($pagos as $pago) {
			$facturas[$pago->id] = $this->Facturas_model->listadoDetallesFactura($pago->id);
		}
		$datos['pagos'] = $pagos;
		$datos['facturas'] = $facturas;
		$datos['titulo'] = 'Lista de pagos';
		$datos['contenido'] = 'pagos/listado_pdf';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-pagos');
	}

	public function planes()
	{
		$datos['hostings'] = $this->Hostings_model->listado();
		$datos['titulo'] = 'Lista de planes de hosting';
		$datos['contenido'] = 'hostings/listado';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-planes');
	}

	public function promociones()
	{
		$datos['promociones'] = $this->Promociones_model->listado();
		$datos['titulo'] = 'Lista de promociones';
		$datos['contenido'] = 'promociones/listado';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-promociones');
	}

	private function renderizar($html, $nombre)
	{
		$dompdf = new Dompdf();
		$dompdf->set_option('isHtml5ParserEnabled', true);
		$dompdf->setPaper('letter', 'landscape');
		// $dompdf->setPaper('letter', 'portrait');
		$dompdf->loadHtml($html);
		$dompdf->render();
		$dompdf->stream($nombre, array('compress' => 1, 'Attachment' => 0));
	}

	public function usuarios()
	{
		$datos['usuarios'] = $this->Usuarios_model->listado();
		$datos['titulo'] = 'Lista de usuarios';
		$datos['contenido'] = 'usuarios/listado';
		$html = $this->load->view('reportes', $datos, TRUE);
		$this->renderizar($html, 'reporte-usuarios');
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('cart');
		$this->load->model('Clientes_model');
		$this->load->model('Dominios_model');
		$this->load->model('Facturas_model');
		$this->load->model('Hostings_model');
		$this->load->model('Iva_model');
		$this->load->model('Pedidos_model');
		$this->load->model('Promociones_model');
	}

	public function actualizar()
	{
		$inputs = $this->input->post();
		$promocion =  $this->input->post('promocion');
		$datos_promocion = $this->Promociones_model->consultarPorCodigo($promocion);

		if (!is_null($datos_promocion)) {
			$this->session->set_userdata('promocion', $promocion);
			$this->session->set_userdata('descuento', $datos_promocion->desc_promo);

		} else {
			$this->session->unset_userdata('promocion');
			$this->session->unset_userdata('descuento');
		}

		foreach ($inputs as $input) {
			if (isset($input['id'])) {

				$id = $input['id'];

				switch ($input['facturacion']) {
					case 'Mensual': {
						$hosting = $this->Hostings_model->consultar($id);
						$precio = $hosting->precio_mensual;
						break;
					}
					case 'Trimestral': {
						$hosting = $this->Hostings_model->consultar($id);
						$precio = $hosting->precio_trimestral;
						break;
					}
					case 'Semestral': {
						$hosting = $this->Hostings_model->consultar($id);
						$precio = $hosting->precio_semestral;
						break;
					}
					case 'Anual': {
						$hosting = $this->Hostings_model->consultar($id);
						$precio = $hosting->precio_anual;
						break;
					}
					case 'Registro': {
						$dominio = $this->Dominios_model->consultar($id);
						$precio = $dominio->precio_registro;
						break;
					}
					case 'Transferencia': {
						$dominio = $this->Dominios_model->consultar($id);
						$precio = $dominio->precio_transferencia;
						break;
					}
					case 'Propio': {
						$dominio = $this->Dominios_model->consultar($id);
						$precio = $dominio->precio_propio;
						break;
					}
				}

				$datos = array(
					'rowid' => $input['rowid'],
					'price' => $precio,
					'facturacion' => $input['facturacion'],
					'dns1' => $input['dns1'],
					'dns2' => $input['dns2'],
					'dns3' => $input['dns3'],
					'dns4' => $input['dns4']
					);
				$this->cart->update($datos);
			}
		}		
		redirect('pedidos/carrito');
	}

	public function agregar()
	{
		$hosting_id = $this->input->post('hosting');
		$dominio = $this->input->post('dominio');
		$extension_id = $this->input->post('extension');

		if ($this->form_validation->run('agregar-carrito') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('pedidos/dominio/' . $hosting_id);
			
		} else {

			if (!is_null($hosting_id)) {
				$hosting = $this->Hostings_model->consultar($hosting_id);

				// Se agrega el hosting
				$carrito_hosting = array(
					'id' => $hosting->id,
					'qty' => 1,
					'price' => $hosting->precio_mensual,
					'name' => $hosting->nombre,
					'description' => $hosting->descripcion,
					'facturacion' => 'Mensual',
					'tipo' => 'hosting',
					'options' => array(
						'Precio Mensual' => 'Mensual', 
						'Precio Trimestral' => 'Trimestral',
						'Precio Semestral' => 'Semestral',
						'Precio Anual' => 'Anual'
						)
					);
				$this->cart->insert($carrito_hosting);
			}

			// Se agrega el dominio
			$datos_dominio = $this->Dominios_model->consultar($extension_id);
			$dominio = 'www.' . $dominio . $datos_dominio->extension;

			$carrito_dominio = array(
				'id' => $datos_dominio->id,
				'qty' => 1,
				'price' => $datos_dominio->precio_registro,
				'name' => $dominio,
				'description' => '',
				'facturacion' => 'Registro',
				'tipo' => 'dominio',
				'dns1' => '',
				'dns2' => '',
				'dns3' => '',
				'dns4' => '',
				'options' => array(
					'Registro' => 'Registro',
					'Transferencia' => 'Transferencia',
					'Propio' => 'Propio',
					)
				);
			$this->cart->insert($carrito_dominio);

			redirect('pedidos/carrito');
		}
	}

	public function carrito()
	{
		$datos['iva'] = $this->Iva_model->consultar();
		$datos['titulo'] = 'Carrito de compras';
		$datos['contenido'] = 'pedidos/carrito';
		$this->load->view('plantilla', $datos);
	}

	public function dominio($hosting_id = null)
	{
		$datos['hosting'] = $hosting_id;
		$datos['dominios'] = $this->Dominios_model->listado();
		$datos['titulo'] = 'Configurar dominio';
		$datos['contenido'] = 'pedidos/agregar_dominio';
		$this->load->view('plantilla', $datos);
	}

	public function eliminar($row_id)
	{
		$this->cart->remove($row_id);
		redirect('pedidos/carrito');
	}

	public function exitoso($factura_id, $pedido_id)
	{
		$pedido = $this->Pedidos_model->consultar($pedido_id);
		$datos['mensaje'] = 'Su pedido se procesó exitosamente. Debe estar atento a su correo. Allí se le enviaran las credenciales de su cuenta cPanel. También se le enviará allí su factura en formato PDF.';

		$datos['detalles_dominios'] = $this->Pedidos_model->consultarDetallesDominios($pedido_id);
		$datos['detalles_planes'] = $this->Pedidos_model->consultarDetallesPlanes($pedido_id);
		$datos['cliente'] = $this->Clientes_model->consultar($pedido->cliente_id);
		$datos['factura'] = $this->Facturas_model->consultar($factura_id);
		$datos['iva'] = $this->Iva_model->consultar();
		$datos['titulo'] = 'Pedido exitoso';
		$datos['contenido'] = 'pedidos/exitoso';
		$this->load->view('plantilla', $datos);
	}

	public function guardar($cliente)
	{
		if ($this->session->userdata('promocion')) {
			$promocion = $this->session->userdata('promocion');
			$datos_promocion = $this->Promociones_model->consultarPorCodigo($promocion);
			$promocion_id = $datos_promocion->id;

		} else {
			$datos_promocion = $this->Promociones_model->consultarPorCodigo(1);
			$promocion_id = 1;
		}

		$datos_pedido = array(
			'fecha' => date('Y-m-d'),
			'promocion_id' => $promocion_id,
			'cliente_id' => $cliente
			);
		$pedido = $this->Pedidos_model->guardar($datos_pedido);

		// Hostings del pedido
		foreach ($this->cart->contents() as $items) {
			if ($items['tipo'] == 'hosting') {

				$fecha_actual = date('Y-m-d');
				switch ($items['facturacion']) {
					case 'Mensual': {
						$fecha_expiracion = strtotime('+1 month', strtotime($fecha_actual));
						break;
					}
					case 'Trimestral': {
						$fecha_expiracion = strtotime('+3 month', strtotime($fecha_actual));
						break;
					}
					case 'Semestral': {
						$fecha_expiracion = strtotime('+6 month', strtotime($fecha_actual));
						break;
					}
					case 'Anual': {
						$fecha_expiracion = strtotime('+1 year', strtotime($fecha_actual));
						break;
					}
				}
				$fecha_expiracion = date('Y-m-d', $fecha_expiracion);

				$detalles_hostings = array(
					'pedidos_id' => $pedido,
					'planes_id' => $items['id'],
					'fecha_expiracion' => $fecha_expiracion,
					'ciclo_facturacion' => $items['facturacion']
					);
				$this->Pedidos_model->guardarDetallesHosting($detalles_hostings);
			}
		}

		// Dominios del pedido
		foreach ($this->cart->contents() as $items) {
			if ($items['tipo'] == 'dominio') {

				$fecha_actual = date('Y-m-d');
				$fecha_expiracion = strtotime('+1 year', strtotime($fecha_actual));
				$fecha_expiracion = date('Y-m-d', $fecha_expiracion);

				$detalles_dominios = array(
					'pedido_id' => $pedido,
					'dominio_id' => $items['id'],
					'dominio' => $items['name'],
					'fecha_expiracion' => $fecha_expiracion,
					'tipo' => $items['facturacion'],
					'dns1' => $items['dns1'],
					'dns2' => $items['dns2'],
					'dns3' => $items['dns3'],
					'dns4' => $items['dns4']
					);
				$this->Pedidos_model->guardarDetallesDominios($detalles_dominios);
			}
		}

		$iva = $this->Iva_model->consultar();

		$sub_total = $this->cart->total();

		if ($this->session->userdata('promocion')) {
			$descuento_porcentaje = $this->session->userdata('descuento');
			$descuento = $sub_total * ($descuento_porcentaje / 100);

		} else {
			$descuento = 0;
			$subtotal_descuento = $sub_total;
		}

		// Factura
		$datos_factura = array(
			'descuento' => $descuento,
			'fecha_creacion' => date('Y-m-d'),
			'fecha_pago' => null,
			'pedidos_id' => $pedido,
			'iva_id' => 1
			);
		$factura = $this->Facturas_model->guardar($datos_factura);
		$this->cart->destroy();
		$this->session->unset_userdata('promocion');
		$this->session->unset_userdata('descuento');

		redirect('pedidos/exitoso/' . $factura . '/' . $pedido);
	}

	public function listado()
	{
		$pedidos = $this->Pedidos_model->listado();

		$dominios = array();
		foreach ($pedidos as $pedido) {
			$dominios[$pedido->id] = $this->Pedidos_model->consultarDetallesDominios($pedido->id);
		}

		$planes = array();
		foreach ($pedidos as $pedido) {
			$planes[$pedido->id] = $this->Pedidos_model->consultarDetallesPlanes($pedido->id);
		}

		$datos['pedidos'] = $pedidos;
		$datos['dominios'] = $dominios;
		$datos['planes'] = $planes;
		$datos['titulo'] = 'Listado de pedidos';
		$datos['contenido'] = 'pedidos/listado';
		$this->load->view('administrador', $datos);
	}

	public function procesar()
	{
		$datos['titulo'] = 'Procesar pedido';
		$datos['contenido'] = 'pedidos/procesar';
		$this->load->view('plantilla', $datos);
	}

	public function vaciar()
	{
		$this->session->unset_userdata('promocion');
		$this->session->unset_userdata('descuento');
		$this->cart->destroy();
		redirect('pedidos/carrito');
	}
}

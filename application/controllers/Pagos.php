<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Bancos_model');
		$this->load->model('Facturas_model');
		$this->load->model('Iva_model');
		$this->load->model('Pagos_model');
		$this->load->model('Pedidos_model');
	}

	public function exitoso($pago_id)
	{
		$pedidos = $this->Pagos_model->datosPedidos($pago_id);

		$datos_planes = array();
		foreach ($pedidos as $pedido) {
			$datos_planes[$pedido->pedidos_id] = $this->Pedidos_model->consultarDetallesPlanes($pedido->pedidos_id);
		}

		$datos_dominios = array();
		foreach ($pedidos as $pedido) {
			$datos_dominios[$pedido->pedidos_id] = $this->Pedidos_model->consultarDetallesDominios($pedido->pedidos_id);
		}

		$datos['pedidos'] = $pedidos;
		$datos['datos_planes'] = $datos_planes;
		$datos['datos_dominios'] = $datos_dominios;
		$datos['titulo'] = 'Pago exitoso';
		$datos['contenido'] = 'pagos/exitoso';
		$this->load->view('plantilla', $datos);
	}

	public function guardar()
	{
		if ($this->form_validation->run('reportar-pago') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('pagos/reportar');
			
		} else {

			$facturas = $this->input->post('factura[]');
			$fecha = $this->input->post('fecha');

			$datos_pago = array(
				'referencia_trans' => $this->input->post('referencia'),
				'monto_pagado' => $this->input->post('monto'),
				'fecha' => $fecha,
				'comentario' => $this->input->post('comentario'),
				'banco_id' => $this->input->post('banco'),
				);
			$pago = $this->Pagos_model->guardar($datos_pago);

			foreach ($facturas as $factura) {
				$datos_detalle = array(
					'factura_id' => $factura,
					'pagos_id' => $pago
					);
				$this->Pagos_model->guardarDetallesFactura($datos_detalle);

				$datos_factura = array(
					'fecha_pago' => $fecha
					);
				$this->Facturas_model->modificar($factura, $datos_factura);
			}
			redirect('pagos/exitoso/' . $pago);
		}
	}

	public function listado()
	{
		$pagos = $this->Pagos_model->listado();

		$facturas = array();
		foreach ($pagos as $pago) {
			$facturas[$pago->id] = $this->Facturas_model->listadoDetallesFactura($pago->id);
		}

		$datos['pagos'] = $pagos;
		$datos['facturas'] = $facturas;
		$datos['titulo'] = 'Pagos realizados';
		$datos['contenido'] = 'pagos/listado';
		$this->load->view('administrador', $datos);
	}

	public function procesar($estado, $pago_id)
	{
		$datos_pagos = array(
			'estado_id' => $estado
			);
		$this->Pagos_model->modificar($pago_id, $datos_pagos);

		if ($estado == 1 || $estado == 2) {
			$facturas = $this->Facturas_model->listadoDetallesFactura($pago_id);

			foreach ($facturas as $factura) {
				$datos = array(
					'estado_id' => $estado
					);
				$this->Facturas_model->modificar($factura->id, $datos);
			}

		}
		redirect('pagos/listado');
	
	}

	public function reportar()
	{
		$cliente_id = $this->session->userdata('cliente_id');
		$facturas = $this->Facturas_model->facturasCliente($cliente_id);
		$iva = $this->Iva_model->consultar();
		$totales_facturas = array();
		foreach ($facturas as $factura) {
			$datos_dominios = $this->Pedidos_model->consultarDetallesDominios($factura->pedido_id);

			$total_dominios = 0;
			foreach ($datos_dominios as $dominio) {
				if ($dominio->tipo == 'Registro') {
					$total_dominios += $dominio->precio_registro;
				}
				if ($dominio->tipo == 'Transferencia') {
					$total_dominios += $dominio->precio_transferencia;
				}
				if ($dominio->tipo == 'Propio') {
					$total_dominios += $dominio->precio_propio;
				}
			}

			$datos_planes = $this->Pedidos_model->consultarDetallesPlanes($factura->pedido_id);
			$total_planes = 0;
			foreach ($datos_planes as $plan) {
				if ($plan->ciclo_facturacion == 'Mensual') {
					$total_planes += $plan->precio_mensual;
				}
				if ($plan->ciclo_facturacion == 'Trimestral') {
					$total_planes += $plan->precio_trimestral;
				}
				if ($plan->ciclo_facturacion == 'Semestral') {
					$total_planes += $plan->precio_semestral;
				}
				if ($plan->ciclo_facturacion == 'Anual') {
					$total_planes += $plan->precio_anual;
				}
			}

			$subtotal = $total_dominios + $total_planes;
			$total_iva = ($subtotal - $factura->descuento) * ($iva->iva / 100);
			$data = array(
				'subtotal' => $subtotal,
				'iva' => $total_iva
				);
			$totales_facturas[$factura->id] = $data;
		}

		$datos['bancos'] = $this->Bancos_model->listado();
		$datos['facturas'] = $facturas;
		$datos['totales_facturas'] = $totales_facturas;
		$datos['titulo'] = 'Reportar pagos';
		$datos['contenido'] = 'pagos/reportar';
		$this->load->view('plantilla', $datos);
	}

}

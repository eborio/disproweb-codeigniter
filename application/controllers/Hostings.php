<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hostings extends CI_Controller
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Hostings_model');
	}

	public function editar($id)
	{
		$datos['hosting'] = $this->Hostings_model->consultar($id);
		$datos['titulo'] = 'Edición de planes de hosting';
		$datos['contenido'] = 'hostings/editar';
		$this->load->view('administrador', $datos);
	}

	public function eliminar($id)
	{
		$this->Hostings_model->eliminar($id);
		$this->Usuarios_model->bitacora('Hostings', 'Eliminar');
		$this->session->set_flashdata('mensaje', 'Hosting eliminado exitosamente');
		redirect('hostings/listado');
	}

	public function guardar()
	{
		if ($this->form_validation->run('registro-hostings') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('hostings/registro');
			
		} else {
			$datos = array(
				'nombre' => $this->input->post('nombre'),
				'espacio' => $this->input->post('espacio'),
				'banda_ancha' => $this->input->post('banda'),
				'descripcion' => $this->input->post('descripcion'),
				'cant_correos' => $this->input->post('correos'),
				'cant_basedatos' => $this->input->post('bases_datos'),
				'cant_subdominios' => $this->input->post('subdominios'),
				'cant_ftp' => $this->input->post('ftp'),
				'precio_mensual' => $this->input->post('mensual'),
				'precio_trimestral' => $this->input->post('trimestral'),
				'precio_semestral' => $this->input->post('semestral'),
				'precio_anual' => $this->input->post('anual'),
				);
			$this->Hostings_model->registrar($datos);
			$this->Usuarios_model->bitacora('Hostings', 'Registrar');
			$this->session->set_flashdata('mensaje', 'Hosting registrado exitosamente');
			redirect('hostings/listado');
		}
	}

	public function index()
	{
		$datos['hostings'] = $this->Hostings_model->listado('precio_mensual', 'asc');
		$datos['titulo'] = 'Planes de Hosting';
		$datos['contenido'] = 'hostings/precios';
		$this->load->view('plantilla', $datos);
	}

	public function listado()
	{
		$datos['hostings'] = $this->Hostings_model->listado();
		$datos['titulo'] = 'Lista de planes de hosting';
		$datos['contenido'] = 'hostings/listado';
		$this->load->view('administrador', $datos);
	}

	public function modificar()
	{
		$id = $this->input->post('id');

		if ($this->form_validation->run('modificar-hostings') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('hostings/editar/' . $id);
			
		} else {
			$datos = array(
				'nombre' => $this->input->post('nombre'),
				'espacio' => $this->input->post('espacio'),
				'banda_ancha' => $this->input->post('banda'),
				'descripcion' => $this->input->post('descripcion'),
				'cant_correos' => $this->input->post('correos'),
				'cant_basedatos' => $this->input->post('bases_datos'),
				'cant_subdominios' => $this->input->post('subdominios'),
				'cant_ftp' => $this->input->post('ftp'),
				'precio_mensual' => $this->input->post('mensual'),
				'precio_trimestral' => $this->input->post('trimestral'),
				'precio_semestral' => $this->input->post('semestral'),
				'precio_anual' => $this->input->post('anual'),
				);
			$this->Hostings_model->modificar($id, $datos);
			$this->Usuarios_model->bitacora('Hostings', 'Modificar');
			$this->session->set_flashdata('mensaje', 'Hosting modificado exitosamente');
			redirect('hostings/editar/' . $id);
		}
	}

	public function registro()
	{
		$datos['titulo'] = 'Registro de planes de hosting';
		$datos['contenido'] = 'hostings/registro';
		$this->load->view('administrador', $datos);
	}

}

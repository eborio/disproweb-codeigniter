<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mantenimiento extends CI_Controller
{
	public function respaldar()
	{
		$datos['titulo'] = 'Respaldar base de datos';
		$datos['zip'] = $this->input->post('zip');
		$datos['contenido'] = 'mantenimiento/respaldar';
		$this->load->view('administrador', $datos);
	}
}

/* End of file respaldos.php */
/* Location: ./application/modules/respaldos/controllers/respaldos.php */
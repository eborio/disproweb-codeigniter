<div class="row">
	<?php
	foreach ($bancos as $banco) {
		?>
		<div class="col-md-3">
			<p>
				<strong>Banco:</strong> <?php echo $banco->nombre;?>
				<br>
				<strong>Cuenta:</strong> <?php echo $banco->nro_cuenta;?>
				<br>
				<strong>Razón social:</strong> <?php echo $banco->razon_social;?>
				<br>
				<strong>RIF:</strong> <?php echo $banco->rif;?>
			</p>
		</div>
		<?php
	}
	?>
</div>
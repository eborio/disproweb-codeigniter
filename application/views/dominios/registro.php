<?php
if ($this->session->flashdata('error')) {
	echo $this->session->flashdata('error');
}
?>
<form action="<?php echo base_url('dominios/guardar');?>" method="post">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<label for="extension">Extensión</label>
				<input type="text" name="extension" id="extension" class="form-control" required>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="precio_registro">Precio de registro</label>
				<input type="text" name="precio_registro" id="precio_registro" class="form-control" required>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="precio_transferencia">Precio de transferencia</label>
				<input type="text" name="precio_transferencia" id="precio_transferencia" class="form-control" required>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="precio_propio">Precio propio</label>
				<input type="text" name="precio_propio" id="precio_propio" class="form-control" required>
			</div>
		</div>
		<div class="col-md-12">
			<button class="btn btn-primary" type="submit">
				<span class="glyphicon glyphicon-ok"></span>
				Registrar
			</button>
		</div>
	</div>
</form>
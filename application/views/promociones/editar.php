<?php
if ($this->session->flashdata('error')) {
	echo $this->session->flashdata('error');
}
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<form action="<?php echo base_url('promociones/modificar');?>" method="post">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="descuento">Descuento</label>
				<div class="input-group">
					<div class="input-group-addon">%</div>
					<input type="text" name="descuento" id="descuento" class="form-control" required value="<?php echo $promocion->desc_promo;?>">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="valido_hasta">Válido hasta</label>
				<input type="date" name="valido_hasta" id="valido_hasta" class="form-control" value="<?php echo $promocion->fecha_valida;?>">
			</div>
		</div>
		<div class="col-md-12">
			<input type="hidden" name="id" value="<?php echo $promocion->id;?>">
			<button class="btn btn-primary" type="submit">
				<span class="glyphicon glyphicon-refresh"></span>
				Actualizar
			</button>
		</div>
	</div>
</form>
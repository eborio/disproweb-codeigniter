<?php
if ($this->session->flashdata('error')) {
	echo $this->session->flashdata('error');
}
?>
<form action="<?php echo base_url('promociones/guardar');?>" method="post">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="descuento">Descuento</label>
				<div class="input-group">
					<div class="input-group-addon">%</div>
					<input type="text" name="descuento" id="descuento" class="form-control" required>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="valido_hasta">Válido hasta</label>
				<input type="date" name="valido_hasta" id="valido_hasta" class="form-control">
			</div>
		</div>
		<div class="col-md-12">
			<button class="btn btn-primary" type="submit">
				<span class="glyphicon glyphicon-ok"></span>
				Registrar
			</button>
		</div>
	</div>
</form>
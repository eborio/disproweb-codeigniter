<?php
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<table class="table table-bordered table-condensed table-striped datatables-promociones">
	<thead>
		<tr>
			<th>Código</th>
			<th>Descuento</th>
			<th>Válido hasta</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($promociones as $promocion) {
			?>
			<tr>
				<td>
					<?php echo $promocion->codigo_cupon;?>
				</td>
				<td>
					<?php echo $promocion->desc_promo;?>
				</td>
				<td>
					<?php 
					if (!empty($promocion->fecha_valida)) {
						echo date('d/m/Y', strtotime($promocion->fecha_valida));
					}
					?>
				</td>
				<td>
					<?php
					if ($this->session->userdata('nivel') == 1) {
						?>
						<a href="<?php echo base_url('promociones/editar/' . $promocion->id);?>" data-tooltip="tooltip" data-placement="top" title="Editar">
							<span class="glyphicon glyphicon-pencil"></span>
						</a>
						<a href="#modal-confirmacion" data-route="<?php echo base_url('promociones/eliminar/' . $promocion->id);?>" data-tooltip="tooltip" data-toggle="modal" title="Eliminar">
							<span class="glyphicon glyphicon-trash"></span>
						</a>
						<?php
					}
					?>
				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>
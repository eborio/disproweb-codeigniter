<form action="<?php echo base_url('pedidos/actualizar');?>" method="post" id="form-carrito" class="form-inline">
    <?php
    if ($this->cart->contents()) {
        ?>
        <div class="row">
            <div class="col-sm-5 text-right">
                <div class="form-group">
                    <label for="promocion">Código promocional</label>
                    <input type="text" name="promocion" id="promocion" class="form-control" value="<?php echo $this->session->userdata('promocion');?>">
                </div>
            </div>
            <div class="col-sm-7">
                <div class="btn-toolbar pull-right">
                    <div class="btn-group">
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-refresh"></span>
                            Actualizar Carrito
                        </button>
                        <a href="<?php echo base_url('pedidos/vaciar')?>" class="btn btn-default">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                            Vaciar Carrito
                        </a>
                        <a href="<?php echo base_url('pedidos/procesar')?>" class="btn btn-default">
                            <span class="glyphicon glyphicon-ok"></span>
                            Procesar Pedido
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <?php
    }
    ?>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-condensed">
            <tr>
                <th>Producto</th>
                <th>Facturación</th>
                <th>DNS</th>
                <th class="text-right">Precio</th>
                <th class="text-right">Subtotal</th>
                <th></th>
            </tr>
            <?php
            if ($this->cart->contents()) {
                $i = 1;
                foreach ($this->cart->contents() as $items) {
                    ?>
                    <input type="hidden" name="<?php echo $i;?>[rowid]" value="<?php echo $items['rowid'];?>">
                    <tr>
                        <td>
                            <b><?php echo $items['name'];?></b>
                            <br>
                            <?php echo $items['description'];?>
                        </td>
                        <td width="20%">
                            <select name="<?php echo $i;?>[facturacion]" class="form-control facturacion">
                                <?php 
                                foreach ($this->cart->product_options($items['rowid']) as $key => $option_value) {
                                    if ($items['facturacion'] === $option_value) {
                                        $selected = 'selected';

                                    } else {
                                        $selected = '';
                                    }
                                    ?>
                                    <option value="<?php echo $option_value;?>" <?php echo $selected;?>>
                                        <?php echo $key;?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                            <input type="hidden" name="<?php echo $i;?>[id]" value="<?php echo $items['id'];?>">
                        </td>
                        <td>
                            <?php
                            if ($items['tipo'] == 'dominio') {
                                ?>
                                <input type="text" name="<?php echo $i;?>[dns1]" value="<?php echo $items['dns1'];?>" class="form-control" placeholder="Ej. ns1.dominio.com">
                                <br>
                                <input type="text" name="<?php echo $i;?>[dns2]" value="<?php echo $items['dns2'];?>" class="form-control" placeholder="Ej. ns2.dominio.com">
                                <br>
                                <input type="text" name="<?php echo $i;?>[dns3]" value="<?php echo $items['dns3'];?>" class="form-control" placeholder="Ej. ns3.dominio.com">
                                <br>
                                <input type="text" name="<?php echo $i;?>[dns4]" value="<?php echo $items['dns4'];?>" class="form-control" placeholder="Ej. ns4.dominio.com">
                                <br>
                                <?php
                            }
                            ?>
                        </td>
                        <td class="text-right">
                            Bs. <?php echo number_format($items['price'], 2, ',', '.'); ?>
                        </td>
                        <td class="text-right">
                            Bs. <?php echo number_format($items['subtotal'], 2, ',', '.'); ?>
                        </td>
                        <td class="text-right">
                            <a href="<?php echo base_url('pedidos/eliminar/' . $items['rowid']);?>" class="btn btn-danger btn-sm">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }

                $subtotal = $this->cart->total();

                if ($this->session->userdata('promocion')) {
                    $descuento_porcentaje = $this->session->userdata('descuento');
                    $descuento = $subtotal * ($descuento_porcentaje / 100);
                    $subtotal_descuento = $subtotal - $descuento;
                    $total_iva = $subtotal_descuento * ($iva->iva / 100);

                } else {
                    $descuento = 0;
                    $subtotal_descuento = $subtotal;
                    $total_iva = $subtotal * ($iva->iva / 100);
                }

                $total = $subtotal + $total_iva;
                
                ?>
                <tr>
                    <td colspan="3"></td>
                    <td class="text-right"><strong>Subtotal</strong></td>
                    <td class="text-right">Bs. <?php echo number_format($subtotal, 2, ',', '.'); ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td class="text-right"><strong>Descuento</strong></td>
                    <td class="text-right">
                        Bs. 
                        <?php
                        echo number_format($descuento, 2, ',', '.');
                        ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td class="text-right"><strong>Subtotal - Descuento</strong></td>
                    <td class="text-right">
                        Bs. 
                        <?php
                        echo number_format($subtotal_descuento, 2, ',', '.');
                        ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td class="text-right"><strong>IVA</strong></td>
                    <td class="text-right">Bs. <?php echo number_format($total_iva, 2, ',', '.'); ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td class="text-right"><strong>Total</strong></td>
                    <td class="text-right">Bs. <?php echo number_format($total - $descuento, 2, ',', '.'); ?></td>
                    <td></td>
                </tr>
                <?php
            }
            else {
                ?>
                <tr>
                    <td colspan="6">
                        <div class="alert alert-danger">
                            <strong>¡Atención!</strong> El carrito de compras está vacío. Agregar un producto
                            <a href="<?php echo base_url();?>" class="btn btn-danger">
                                Ver productos
                            </a>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
</form>
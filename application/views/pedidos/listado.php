<table class="table table-bordered table-condensed table-striped datatables-usuarios" border="1">
	<thead>
		<tr>
			<th>ID</th>
			<th>Fecha</th>
			<th>Cliente</th>
			<th>Dominios</th>
			<th>Planes</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($pedidos as $pedido) {
			?>
			<tr>
				<td>
					<?php echo $pedido->id;?>
				</td>
				<td>
					<?php echo date('d-m-Y', strtotime($pedido->fecha));?>
				</td>
				<td>
					<b>Cédula:</b> <?php echo $pedido->cedula;?>
					<br>
					<b>Nombre:</b> <?php echo $pedido->nombre;?>
					<br>
					<b>Apellido:</b> <?php echo $pedido->apellido;?>
				</td>
				<td>
					<?php
					foreach ($dominios[$pedido->id] as $dominio) {
						?>
						<b>Dominio: </b> <?php echo $dominio->dominio;?>
						<br>
						<b>Expiración: </b> <?php echo date('d-m-Y', strtotime($dominio->fecha_expiracion));?>
						<br>
						<b>Tipo: </b> <?php echo $dominio->tipo;?>
						<br><br>
						<?php
					}
					?>
				</td>
				<td>
					<?php
					foreach ($planes[$pedido->id] as $plan) {
						?>
						<b>Nombre: </b> <?php echo $plan->nombre;?>
						<br>
						<b>Expiración: </b> <?php echo date('d-m-Y', strtotime($plan->fecha_expiracion));?>
						<br>
						<b>Facturación: </b> <?php echo $plan->ciclo_facturacion;?>
						<br><br>
						<?php
					}
					?>
				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>
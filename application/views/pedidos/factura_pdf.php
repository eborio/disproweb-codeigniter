<div class="invoice-content-2">
	<div class="row invoice-head">
		<div class="col-md-5 col-xs-6">
			<div class="derecha">
				<span class="bold uppercase">Disproweb C.A.</span>
				<br>
				<span class="bold uppercase">Factura Nº <?php echo $factura->id;?></span>
				<br>
				RIF J-40101414-3
				<br>
				Carrera 17, entre calles 16 y 17
				<br>
				Barquisimeto, Lara
				<br>
				0251 414 6777
				<br>
				info@disproweb.net
				<br>
				www.disproweb.net
			</div>
		</div>
		<table>
			<tr>
				<th>Cliente</th>
				<th>Fecha</th>
				<th>Dirección</th>
			</tr>
			<tr>
				<td><?php echo $cliente->nombre . ' ' . $cliente->apellido;?></td>
				<td><?php echo date('d-m-Y', strtotime($factura->fecha_creacion));?></td>
				<td><?php echo $cliente->direccion;?></td>
			</tr>
		</table>
		<div class="row invoice-body">
			<div class="col-xs-12 table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Producto</th>
							<th class="derecha">Precio</th>
							<th class="derecha">Subtotal</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$subtotal_dominios = 0;
						foreach ($detalles_dominios as $dominio) {
							?>
							<tr>
								<td>
									<p>
										<?php echo $dominio->dominio;?> (<?php echo $dominio->tipo;?>)
									</p>
								</td>
								<td class="derecha">
									<?php
									switch ($dominio->tipo) {
										case 'Registro': {
											$precio_dominio = $dominio->precio_registro;
											break;
										}
										case 'Transferencia': {
											$precio_dominio = $dominio->precio_transferencia;
											break;
										}
										case 'Propio': {
											$precio_dominio = $dominio->precio_propio;
											break;
										}
									}
									?>
									Bs. <?php echo number_format($precio_dominio, 2, ',', '.');?>
								</td>
								<td class="derecha">
									<?php
									$subtotal_dominios += $precio_dominio;
									?>
									Bs. <?php echo number_format($precio_dominio, 2, ',', '.');?>
								</td>
							</tr>
							<?php
						}
						$subtotal_planes = 0;
						foreach ($detalles_planes as $plan) {
							?>
							<tr>
								<td>
									<p>
										<?php echo $plan->nombre;?> (<?php echo $plan->ciclo_facturacion;?>)
									</p>
								</td>
								<td class="derecha">
									<?php
									switch ($plan->ciclo_facturacion) {
										case 'Mensual': {
											$precio_plan = $plan->precio_mensual;
											break;
										}
										case 'Trimestral': {
											$precio_plan = $plan->precio_trimestral;
											break;
										}
										case 'Semestral': {
											$precio_plan = $plan->precio_semestral;
											break;
										}
										case 'Anual': {
											$precio_plan = $plan->precio_anual;
											break;
										}
									}
									?>
									Bs. <?php echo number_format($precio_plan, 2, ',', '.');?>
								</td>
								<td class="derecha">
									<?php
									$subtotal_planes += $precio_plan;
									?>
									Bs. <?php echo number_format($precio_plan, 2, ',', '.');?>
								</td>
							</tr>
							<?php
						}
						$subtotal_general = $subtotal_dominios + $subtotal_planes;
						?>
					</tbody>
				</table>
			</div>
		</div>
		<table>
			<tr>
				<td class="derecha" width="90%">
					<b>Subtotal</b>
				</td>
				<td class="derecha">Bs. <?php echo number_format($subtotal_general, 2, ',', '.');?></td>
			</tr>
			<tr>
				<td class="derecha">
					<b>Descuento</b>
				</td>
				<td class="derecha">Bs. <?php echo number_format($factura->descuento, 2, ',', '.'); ?></td>
			</tr>
			<tr>
				<td class="derecha">
					<b>Subtotal - Descuento</b>
				</td>
				<?php
				$subotal_descuento = $subtotal_general - $factura->descuento;
				?>
				<td class="derecha">Bs. <?php echo number_format($subotal_descuento, 2, ',', '.'); ?></td>
			</tr>
			<tr>
				<td class="derecha">
					<b>IVA</b>
				</td>
				<?php
				$total_iva = $subotal_descuento * ($iva->iva / 100);
				?>
				<td class="derecha">Bs. <?php echo number_format($total_iva, 2, ',', '.'); ?></td>
			</tr>
			<tr>
				<td class="derecha">
					<b>Total</b>
				</td>
				<td class="derecha">Bs. <?php echo number_format($subtotal_general + $total_iva - $factura->descuento, 2, ',', '.'); ?></td>
			</tr>
		</table>
	</div>
</div>
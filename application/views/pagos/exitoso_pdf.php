<table class="table table-bordered table-condensed table-striped" border="1">
	<tr>
		<th>
			Datos de los planes de hosting
		</th>
		<th>
			Datos de los dominios
		</th>
	</tr>
	<?php
	foreach ($pedidos as $pedido) {
		?>
		<tr>
			<td>
				<?php
				foreach ($datos_planes[$pedido->pedidos_id] as $plan) {
					?>
					<strong>Nombre del plan: </strong> <?php echo $plan->nombre;?><br>
					<strong>Descripcion del plan: </strong> <?php echo $plan->descripcion;?><br>
					<strong>Ciclo de facturación: </strong> <?php echo $plan->ciclo_facturacion;?><br>
					<strong>Espacio en disco: </strong> GB <?php echo $plan->espacio;?><br>
					<strong>Ancho de banda: </strong> GB <?php echo $plan->banda_ancha;?><br>
					<strong>Correos: </strong> <?php echo $plan->cant_correos;?><br>
					<strong>Subdominios: </strong> <?php echo $plan->cant_subdominios;?><br>
					<strong>Bases de datos: </strong> <?php echo $plan->cant_basedatos;?><br>
					<strong>Cuentas FTP: </strong> <?php echo $plan->cant_ftp;?><br>
					<strong>Fecha de expiración: </strong> <?php echo date('d-m-Y', strtotime($plan->fecha_expiracion));?><br>
					<?php
				}
				?>
			</td>
			<td>
				<?php
				foreach ($datos_dominios[$pedido->pedidos_id] as $dominio) {
					?>
					<strong>Dominio: </strong> <?php echo $dominio->dominio;?><br>
					<strong>Fecha de expiración: </strong> <?php echo date('d-m-Y', strtotime($dominio->fecha_expiracion));?><br>
					<strong>DNS1: </strong> <?php echo $dominio->dns1;?><br>
					<strong>DNS2: </strong> <?php echo $dominio->dns2;?><br>
					<strong>DNS3: </strong> <?php echo $dominio->dns3;?><br>
					<strong>DNS4: </strong> <?php echo $dominio->dns4;?><br>
					<?php
				}
				?>
			</td>
		</tr>
		<?php
	}
	?>
</table>
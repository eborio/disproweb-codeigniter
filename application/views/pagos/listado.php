<?php
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<table class="table table-bordered table-hover table-striped table-condensed datatables-pagos">
	<thead>
		<tr>
			<th>Referencia</th>
			<th>Fecha</th>
			<th>Comentario</th>
			<th>Banco</th>
			<th>Facturas pagadas</th>
			<th class="text-right">Monto</th>
			<th>Estado</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($pagos as $pago) {
			?>
			<tr>
				<td>
					<?php echo $pago->referencia_trans;?>
				</td>
				<td>
					<?php echo date('d-m-Y', strtotime($pago->fecha));?>
				</td>
				<td>
					<?php echo $pago->comentario;?>
				</td>
				<td>
					<?php echo $pago->nombre . ' (' . $pago->nro_cuenta . ')';?>
				</td>
				<td>
					<ul>
						<?php
						foreach ($facturas[$pago->id] as $factura) {
							?>
							<li>
								#<?php echo $factura->id;?>
							</li>
							<?php
						}
						?>
					</ul>
				</td>
				<td class="text-right">
					Bs. <?php echo number_format($pago->monto_pagado, 2, ',', '.');?>
				</td>
				<td>
					<?php
					if ($pago->estado_pago == 1) {
						?>
						<label class="label label-primary">Pendiente</label>
						<?php
					}
					if ($pago->estado_pago == 2) {
						?>
						<label class="label label-success">Aprobado</label>
						<?php
					}
					if ($pago->estado_pago == 3) {
						?>
						<label class="label label-danger">Rechazado</label>
						<?php
					}
					?>
				</td>
				<td>
					<?php
					if ($pago->estado_pago == 2 || $pago->estado_pago == 3) {
						?>
						<a href="<?php echo base_url('pagos/procesar/1/' . $pago->id);?>" class="btn btn-primary btn-xs">
							Colocar pendiente
						</a>
						<?php
					} else {
						?>
						<a href="<?php echo base_url('pagos/procesar/2/' . $pago->id);?>" class="btn btn-success btn-xs">
							Aprobar
						</a>
						<a href="<?php echo base_url('pagos/procesar/3/' . $pago->id);?>" class="btn btn-danger btn-xs">
							Rechazar
						</a>
						<?php
					}
					?>

				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>
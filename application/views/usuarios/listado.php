<?php
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<table class="table table-bordered table-condensed table-striped datatables-usuarios">
	<thead>
		<tr>
			<th>ID</th>
			<th>Usuario</th>
			<th>Correo</th>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>Nivel de acceso</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($usuarios as $usuario) {
			?>
			<tr>
				<td>
					<?php echo $usuario->id;?>
				</td>
				<td>
					<?php echo $usuario->usuario;?>
				</td>
				<td>
					<?php echo $usuario->correo;?>
				</td>
				<td>
					<?php echo $usuario->nombre;?>
				</td>
				<td>
					<?php echo $usuario->apellido;?>
				</td>
				<td>
					<?php echo $usuario->nombre_nivel;?>
				</td>
				<td>
					<a href="<?php echo base_url('usuarios/editar/' . $usuario->id);?>" data-tooltip="tooltip" data-placement="top" title="Editar">
						<span class="glyphicon glyphicon-pencil"></span>
					</a>
					<a href="#modal-confirmacion" data-route="<?php echo base_url('usuarios/eliminar/' . $usuario->id);?>" data-tooltip="tooltip" data-toggle="modal" data-placement="top" title="Eliminar">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>
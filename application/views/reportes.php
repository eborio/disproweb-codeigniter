<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<style>
		table {
			width: 100%;
			max-width: 100%;
			margin-bottom: 20px;
			border-spacing: 0;
			border-collapse: collapse;
			border: 1px solid #ddd;
		}
		table th {
			text-align: left;
		}
		.izquierda {
			text-align: left;
		}
		.derecha {
			text-align: right;
		}
	</style>
</head>
<body>
	<h3><?php echo $titulo;?></h3>
	<?php
	$this->load->view($contenido);
	?>
</body>
</html>
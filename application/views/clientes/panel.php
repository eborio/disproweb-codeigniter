<div class="row">
	<div class="col-md-12">
		<h3>Facturas generadas</h3>
		<table class="table table-bordered table-condensed table-striped datatables-facturas">
			<thead>
				<tr>
					<th>Número</th>
					<th>Fecha</th>
					<th>Estado</th>
					<th>Subtotal</th>
					<th>Descuento</th>
					<th>IVA</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($facturas as $factura) {
					?>
					<tr>
						<td><?php echo $factura->id;?></td>
						<td>
							<?php echo date('d-m-Y', strtotime($factura->fecha_creacion));?>
						</td>
						<td>
							<?php
							switch ($factura->estado_id) {
								case '1': {
									echo 'Pendiente';
									break;
								}
								case '2': {
									echo 'Pagada';
									break;
								}
								case '3': {
									echo 'Cancelada';
									break;
								}
							}
							?>
						</td>
						<td class="text-right">
							<?php
							$subtotal = $totales_facturas[$factura->id]['subtotal'];
							?>
							Bs. <?php echo number_format($subtotal, 2, ',', '.');?>
						</td>
						<td class="text-right">
							Bs. <?php echo number_format($factura->descuento, 2, '.', ',');?>
						</td>
						<td class="text-right">
							<?php
							$total_iva = $totales_facturas[$factura->id]['iva'];
							?>
							Bs. <?php echo number_format($total_iva, 2, '.', ',');?>
						</td>
						<td class="text-right">
							Bs. <?php echo number_format($subtotal - $factura->descuento + $total_iva, 2, '.', ',');?>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
	<div class="col-md-12">
		<h3>Pagos realizados</h3>
		<table class="table table-bordered table-hover table-striped table-condensed datatables-pagos">
			<thead>
				<tr>
					<th>Referencia</th>
					<th>Fecha</th>
					<th>Comentario</th>
					<th>Banco</th>
					<th>Facturas pagadas</th>
					<th class="text-right">Monto</th>
					<th>Estado</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($pagos as $pago) {
					?>
					<tr>
						<td>
							<?php echo $pago->referencia_trans;?>
						</td>
						<td>
							<?php echo date('d-m-Y', strtotime($pago->fecha));?>
						</td>
						<td>
							<?php echo $pago->comentario;?>
						</td>
						<td>
							<?php echo $pago->nombre . ' (' . $pago->nro_cuenta . ')';?>
						</td>
						<td>
							<ul>
								<?php
								foreach ($facturas_pagadas[$pago->id] as $factura) {
									?>
									<li>
										#<?php echo $factura->id;?>
									</li>
									<?php
								}
								?>
							</ul>
						</td>
						<td class="text-right">
							Bs. <?php echo number_format($pago->monto_pagado, 2, ',', '.');?>
						</td>
						<td>
							<?php
							if ($pago->estado_pago == 1) {
								?>
								<label class="label label-primary">Pendiente</label>
								<?php
							}
							if ($pago->estado_pago == 2) {
								?>
								<label class="label label-success">Aprobado</label>
								<?php
							}
							if ($pago->estado_pago == 3) {
								?>
								<label class="label label-danger">Rechazado</label>
								<?php
							}
							?>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
</div>
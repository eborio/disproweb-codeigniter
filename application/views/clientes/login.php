<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<?php
		if ($this->session->flashdata('error')) {
			?>
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $this->session->flashdata('error');?>
			</div>
			<?php
		}
		?>
		<form action="<?php echo base_url('clientes/login');?>" method="post" class="well">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label for="usuario">Usuario</label>
						<input type="text" name="usuario" id="usuario" class="form-control" required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="password">Contraseña</label>
						<input type="password" name="password" id="password" class="form-control" required>
					</div>
				</div>
				<div class="col-md-12">
					<button class="btn btn-primary" type="submit">
						<span class="glyphicon glyphicon-ok"></span>
						Iniciar sesión
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
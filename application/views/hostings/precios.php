<div class="pricing-content-1">
    <div class="row">
        <?php
        foreach ($hostings as $hosting) {
            ?>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="price-column-container border-active">
                    <div class="price-table-head bg-purple">
                        <h2 class="no-margin"><?php echo $hosting->nombre;?></h2>
                    </div>
                    <div class="arrow-down border-top-purple"></div>
                    <div class="price-table-pricing">
                        <h3>
                            <span class="price-sign">Bs</span> 
                            <?php
                            echo $hosting->precio_mensual;
                            ?>
                        </h3>
                        <p>
                            por mes
                        </p>
                    </div>
                    <div class="price-table-content">
                        <div class="row mobile-padding">
                            <div class="col-xs-3 text-right mobile-padding">
                                <i class="icon-drawar"></i>
                            </div>
                            <div class="col-xs-9 text-left mobile-padding">
                                <?php
                                if ($hosting->espacio > 0) {
                                    echo $hosting->espacio . ' GB de espacio';

                                } else {
                                    echo 'Ilimitado';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row mobile-padding">
                            <div class="col-xs-3 text-right mobile-padding">
                                <i class="icon-chart"></i>
                            </div>
                            <div class="col-xs-9 text-left mobile-padding">
                                <?php
                                if ($hosting->banda_ancha > 0) {
                                    echo $hosting->banda_ancha . ' GB de Banda';

                                } else {
                                    echo 'Ilimitada';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row mobile-padding">
                            <div class="col-xs-3 text-right mobile-padding">
                                <i class="icon-envelope-open"></i>
                            </div>
                            <div class="col-xs-9 text-left mobile-padding">
                                <?php
                                if ($hosting->cant_correos > 0) {
                                    echo $hosting->cant_correos . ' Correos';

                                } else {
                                    echo 'Ilimitados';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row mobile-padding">
                            <div class="col-xs-3 text-right mobile-padding">
                                <i class="icon-layers"></i>
                            </div>
                            <div class="col-xs-9 text-left mobile-padding">
                                <?php
                                if ($hosting->cant_subdominios > 0) {
                                    echo $hosting->cant_subdominios . ' Subdominios';

                                } else {
                                    echo 'Ilimitados';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row mobile-padding">
                            <div class="col-xs-3 text-right mobile-padding">
                                <i class="icon-docs"></i>
                            </div>
                            <div class="col-xs-9 text-left mobile-padding">
                                <?php
                                if ($hosting->cant_ftp > 0) {
                                    echo $hosting->cant_ftp . ' Cuentas FTP';

                                } else {
                                    echo 'Ilimitadas';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="arrow-down arrow-grey"></div>
                    <div class="price-table-footer">
                        <a href="<?php echo base_url('pedidos/dominio/' . $hosting->id);?>" class="btn red price-button sbold uppercase">
                            Contratar
                        </a>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
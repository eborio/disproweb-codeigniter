<?php
if ($this->session->flashdata('error')) {
	echo $this->session->flashdata('error');
}
?>
<form action="<?php echo base_url('hostings/guardar');?>" method="post">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" id="nombre" class="form-control" required>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="espacio">Espacio en disco</label>
				<div class="input-group">
					<span class="input-group-addon">GB</span>
					<input type="number" name="espacio" id="espacio" class="form-control" required min="0">
				</div>
				<p class="help-block">
					Coloque 0 para ilimitado
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="banda">Ancho de banda</label>
				<div class="input-group">
					<span class="input-group-addon">GB</span>
					<input type="number" name="banda" id="banda" class="form-control" required min="0">
				</div>
				<p class="help-block">
					Coloque 0 para ilimitada
				</p>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label for="descripcion">Descripción</label>
				<textarea name="descripcion" id="descripcion" cols="30" rows="5" class="form-control" required></textarea>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="correos">Cantidad de correos</label>
				<input type="number" name="correos" id="correos" class="form-control" required min="0">
				<p class="help-block">
					Coloque 0 para ilimitados
				</p>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="bases_datos">Cantidad de bases de datos</label>
				<input type="number" name="bases_datos" id="bases_datos" class="form-control" required min="0">
				<p class="help-block">
					Coloque 0 para ilimitadas
				</p>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="subdominios">Cantidad de subdominios</label>
				<input type="number" name="subdominios" id="subdominios" class="form-control" min="0" required>
				<p class="help-block">
					Coloque 0 para ilimitados
				</p>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="ftp">Cantidad de cuentas FTP</label>
				<input type="number" name="ftp" id="ftp" class="form-control" min="0" required>
				<p class="help-block">
					Coloque 0 para ilimitadas
				</p>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="mensual">Precio mensual</label>
				<input type="text" name="mensual" id="mensual" class="form-control" required>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="trimestral">Precio trimestral</label>
				<input type="text" name="trimestral" id="trimestral" class="form-control" required>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="semestral">Precio semestral</label>
				<input type="text" name="semestral" id="semestral" class="form-control" required>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="anual">Precio anual</label>
				<input type="text" name="anual" id="anual" class="form-control" required>
			</div>
		</div>

		<div class="col-md-12">
			<button class="btn btn-primary" type="submit">
				<span class="glyphicon glyphicon-ok"></span>
				Registrar
			</button>
		</div>
	</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-confirmacion">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmar la acción</h4>
			</div>
			<div class="modal-body">
				Debe confirmar la acción para continuar
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<a href="" class="btn btn-danger" id="confirmar">Confirmar</a>
			</div>
		</div>
	</div>
</div>
<!-- Fin del Modal -->
<footer class="page-prefooter">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6 col-xs-12 footer-block">
				<h2>Disproweb</h2>
				<p>
					Somos una empresa dedicada a prestar servicios de hospedaje web de alta calidad que se ajustan a las necesidades de su empresa al mejor precio del mercado.
				</p>
			</div>
		</div>
	</div>
</footer>
<div class="page-footer">
	<div class="container">
		2016 &copy; Disproweb.
	</div>
</div>
<div class="scroll-to-top">
	<i class="icon-arrow-up"></i>
</div>
<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/jquery.mask.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/funciones.js');?>"></script>
<script src="<?php echo base_url('assets/js/funciones_administrador.js');?>"></script>
</body>
</html>
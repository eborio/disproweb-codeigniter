<?php
if ($this->session->flashdata('error')) {
	echo $this->session->flashdata('error');
}
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<form action="<?php echo base_url('iva/modificar');?>" method="post">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="iva">IVA</label>
				<div class="input-group">
					<div class="input-group-addon">%</div>
					<input type="text" name="iva" id="iva" class="form-control" required value="<?php echo $iva->iva;?>">
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<input type="hidden" name="id"  value="<?php echo $iva->id;?>">
			<button class="btn btn-primary" type="submit">
				<span class="glyphicon glyphicon-refresh"></span>
				Modificar
			</button>
		</div>
	</div>
</form>
<table class="table table-bordered table-condensed table-striped datatables-facturas">
	<thead>
		<tr>
			<th>Número</th>
			<th>Fecha</th>
			<th>Estado</th>
			<th>Subtotal</th>
			<th>Descuento</th>
			<th>IVA</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($facturas as $factura) {
			?>
			<tr>
				<td><?php echo $factura->id;?></td>
				<td>
					<?php echo date('d-m-Y', strtotime($factura->fecha_creacion));?>
				</td>
				<td>
					<?php
					switch ($factura->estado_id) {
						case '1': {
							echo 'Pendiente';
							break;
						}
						case '2': {
							echo 'Pagada';
							break;
						}
						case '3': {
							echo 'Cancelada';
							break;
						}
					}
					?>
				</td>
				<td class="text-right">
					<?php
					$subtotal = $totales_facturas[$factura->id]['subtotal'];
					?>
					Bs. <?php echo number_format($subtotal, 2, ',', '.');?>
				</td>
				<td class="text-right">
					Bs. <?php echo number_format($factura->descuento, 2, '.', ',');?>
				</td>
				<td class="text-right">
					<?php
					$total_iva = $totales_facturas[$factura->id]['iva'];
					?>
					Bs. <?php echo number_format($total_iva, 2, '.', ',');?>
				</td>
				<td class="text-right">
					Bs. <?php echo number_format($subtotal - $factura->descuento + $total_iva, 2, '.', ',');?>
				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>